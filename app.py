from cassandra.auth import PlainTextAuthProvider
from cassandra.cluster import Cluster
from cassandra.query import BatchStatement
from flask import *
try:
    from flask import _app_ctx_stack as stack
except ImportError:
    from flask import _request_ctx_stack as stack
import logging
import tflearn, numpy as np, random, nltk, json
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

HOST = "10.194.176.57"
BASE_URL = "/nl/api/v1/"
unicode = str
log = logging.getLogger(__name__)
app = Flask(__name__)
factory = StemmerFactory()
stemmer = factory.create_stemmer()
words = []
classes = []
documents = []
ignore_words = ['?']

class CassandraCluster(object):
    def __init__(self, app=None):
        self.app = app
        self.cluster = None
        if app is not None:
            self.init_app(app)
    def init_app(self, app):
        app.config.setdefault('CASSANDRA_CLUSTER', ':memory:')
        if hasattr(app, 'teardown_appcontext'):
            app.teardown_appcontext(self.teardown)
        else:
            app.teardown_request(self.teardown)
    def connect(self):
        log.debug("Connecting to CASSANDRA NODES {}".format(current_app.config['CASSANDRA_NODES']))
        if self.cluster is None:
            if isinstance(current_app.config['CASSANDRA_NODES'], (list, tuple)):
                self.cluster = Cluster(current_app.config['CASSANDRA_NODES'])
            elif isinstance(current_app.config['CASSANDRA_NODES'], (str, unicode)):
                self.cluster = Cluster([current_app.config['CASSANDRA_NODES']])
            else:
                raise TypeError("CASSANDRA_NODES must be defined as a list, tuple, string, or unicode object.")

        auth_provider = PlainTextAuthProvider(username='cassandra', password='cassandra')
        self.cluster = Cluster([HOST],protocol_version=3, auth_provider=auth_provider)
        self.session = self.cluster.connect()
        return self.session
    def teardown(self, exception):
        ctx = stack.top
        if hasattr(ctx, 'cassandra_cluster'):
            ctx.cassandra_cluster.shutdown()
    @property
    def connection(self):
        ctx = stack.top
        if ctx is not None:
            if not hasattr(ctx, 'cassandra_cluster'):
                ctx.cassandra_cluster = self.connect()
            return ctx.cassandra_cluster

cassandra = CassandraCluster()
app.config['CASSANDRA_NODES'] = ['cassandra-c1.terbiumlabs.com']

def _net(train_x, train_y):
    net = tflearn.input_data(shape=[None, len(train_x[0])])
    net = tflearn.fully_connected(net, 8)
    net = tflearn.fully_connected(net, 8)
    net = tflearn.fully_connected(net, len(train_y[0]), activation='softmax')
    net = tflearn.regression(net)
    return net;

def _model(train_x, train_y):
    model = tflearn.DNN(_net(train_x, train_y), tensorboard_dir='tflearn_logs')
    return model;

def loadModel(train_x, train_y):
    model = _model(train_x, train_y)
    model.load('model.tflearn')
    return model;

def clean_up_sentence(sentence):
    sentence_words = nltk.word_tokenize(sentence)
    sentence_words = [stemmer.stem(word.lower()) for word in sentence_words]
    return sentence_words

def bow(sentence, words, show_details=False):
    sentence_words = clean_up_sentence(sentence)
    bag = [0]*len(words)
    for s in sentence_words:
        for i,w in enumerate(words):
            if w == s:
                bag[i] = 1
                if show_details:
                    print ("found in bag: %s" % w)
    return(np.array(bag))

def classify(sentence, model, threshold):
    results = model.predict([bow(sentence, words)])[0]
    results = [[i,r] for i,r in enumerate(results) if r>threshold]
    results.sort(key=lambda x: x[1], reverse=True)
    data={}; res={}
    data['output'] = res
    res['result'] = []
    for r in results:
        res['result'].append({
            'intent': classes[r[0]],
            'confidence': r[1]
        })
    data["input"] = sentence
    return data

def create_nlu(name):
    session = cassandra.connect()
    session.set_keyspace("bot_nl")
    insert_data = session.prepare("INSERT INTO bot_nl.nlu (nluid, name, type) VALUES(now(), ?, ?)")
    batch = BatchStatement()
    batch.add(insert_data, (name, ''))
    session.execute(batch)
    return "success";

def delete_nlu(nlu_id):
    session = cassandra.connect()
    session.set_keyspace("bot_nl")
    session.execute("""
                    DELETE FROM  bot_nl.nlu WHERE nluid = %s ;
                    """ %nlu_id)
    return "delete_nlu "+nlu_id+" success";

def create_entity(nluid, name):
    session = cassandra.connect()
    session.set_keyspace("bot_nl")
    insert_data = session.prepare("INSERT INTO bot_nl.entity (id, nluid, name) VALUES(now(), ?, ?)")
    batch = BatchStatement()
    batch.add(insert_data, (nluid, name))
    session.execute(batch)
    return "success";

def add_label(entityid, tag):
    session = cassandra.connect()
    session.set_keyspace("bot_nl")
    insert_data = session.prepare("INSERT INTO bot_nl.label (id, entityid, tag) VALUES(0, ?, ?)")
    batch = BatchStatement()
    batch.add(insert_data, (entityid, tag))
    session.execute(batch)
    return "success";

def return_result(result):
    ret = {}
    if result:
        ret["code"] = 200
        ret["message"] = "OK"
    else:
        ret["code"] = 404
        ret["message"] = "FAILURE"
    return json.dumps(ret)

@app.route(BASE_URL+"createNLU", methods = ['POST'])
def createModel():
    json_data = request.get_json()
    result = create_nlu(json_data["data"]["name"])
    return Response(return_result(result), mimetype="application/json")

@app.route(BASE_URL+"deleteNLU", methods = ['POST'])
def deleteModel():
    json_data = request.get_json()
    result = delete_nlu(json_data["data"]["nlu_id"])
    return Response(return_result(result), mimetype="application/json")

@app.route(BASE_URL+"createEntity", methods = ['POST'])
def createEntity():
    json_data = request.get_json()
    result = create_entity(json_data["data"]["nlu_id"], json_data["data"]["name"])
    return Response(return_result(result), mimetype="application/json")

@app.route(BASE_URL+"addLabel", methods = ['POST'])
def addLabel():
    json_data = request.get_json()
    result = add_label(json_data["data"][""], json_data["data"][""])
    return Response(return_result(result), mimetype="application/json")

if __name__ == '__main__':
    app.run(debug=True, port=5001)